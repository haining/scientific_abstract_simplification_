#!/usr/bin/env python
# coding=utf-8

__author__ = "hw56@indiana.edu"
# __version__ = "alpha"
__license__ = "ISC"


"""
Current problems (July 27):
1. bleu score could be miscalculated, to be checked
2. setting the upper limits of ckpts seems not working
"""


import os
import torch
import datasets
import numpy as np
import pytorch_lightning as pl
from torch.utils.data import Dataset, DataLoader
from pytorch_lightning.loggers import WandbLogger
from transformers import T5TokenizerFast, T5ForConditionalGeneration
from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor, EarlyStopping, DeviceStatsMonitor

# constants
RUN_NAME = 't5-efficient-tiny-nl32_prototyping_al_1'
MODEL_OR_CKPT_NAME = 'google/t5-efficient-tiny-nl32'
HF_COCHRANE = 'GEM/cochrane-simplification'
CKPTS_DIR = 'ckpts'
PROJECT_NAME = "scientific_abstract_simplification"

# to log generated texts
COLUMNS = ['generated_text', 'complex_text', 'simple_text']
PATIENCE = 1000
BATCH_SIZE = 8
MAX_EPOCHS = 500

# Cochrane is a database of systematic reviews of clinical questions, many of which have summaries in plain English
# targeting readers without a university education. The dataset comprises about 4,500 of such pairs.
cochrane_simplification = datasets.load_dataset("GEM/cochrane-simplification")


pl.seed_everything(42)

# init logger
wandb_logger = WandbLogger(name=RUN_NAME,
                           project=PROJECT_NAME,
                           log_model='all',
                           save_dir='wandb')


class CochraneDataset(Dataset):
    def __init__(
            self,
            data: list,  # a list of dicts of paired texts, with values keyed by `source` and `target`
            tokenizer: str = MODEL_OR_CKPT_NAME,
            task_prefix: str = 'denoise: ',
            max_source_length: int = 512,
            max_target_length: int = 512,
            source_lang: str = 'source',
            target_lang: str = 'target',
            padding: str = 'max_length'):
        self.data = data
        self.tokenizer = T5TokenizerFast.from_pretrained(tokenizer)
        self.task_prefix = task_prefix
        self.max_source_length = max_source_length
        self.max_target_length = max_target_length
        self.source_lang = source_lang
        self.target_lang = target_lang
        self.padding = padding

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index: int):
        datum = self.data[index]
        target = datum[self.target_lang]
        source = datum[self.source_lang]
        encoding = self.tokenizer(self.task_prefix + source,
                                  max_length=self.max_source_length,
                                  padding=self.padding,
                                  truncation=True,
                                  return_tensors='pt'
                                  )
        target_encoding = self.tokenizer(target, max_length=self.max_source_length,
                                         padding=self.padding, truncation=True)
        # replace all tokenizer.pad_token_id in the labels by -100 when we want to ignore padding in the loss
        labels = torch.tensor(
            [(label if label != self.tokenizer.pad_token_id else -100) for label in target_encoding['input_ids']],
            dtype=torch.int64)

        return dict(
            input_ids=encoding['input_ids'].flatten(),
            attention_mask=encoding['attention_mask'].flatten(),
            labels=labels.flatten(),
            target=target,  # for easily computing metrics
            source=source,
        )


class CochraneDataModule(pl.LightningDataModule):
    """
    DataModule for a denoising autoencoder used for misspellings correction.
    It loads huggingface format dataset ('datasets.dataset_dict.DatasetDict') from disk and return train/val/test
    dataloaders.
    """

    def __init__(
            self,
            tokenizer: str = 'google/t5-efficient-tiny-nl32',
            hf_dataset_name: str = HF_COCHRANE,
            batch_size: int = BATCH_SIZE,
            task_prefix: str = 'simplify: ',
            max_source_length: int = 512,
            max_target_length: int = 512,
            source_lang: str = 'source',
            target_lang: str = 'target',
            padding: str = 'max_length',
            #num_workers: int = os.cpu_count(),
            num_workers: int = 0,
    ):
        super().__init__()
        self.dataset = datasets.load_dataset(hf_dataset_name)
        self.batch_size = batch_size
        self.tokenizer = tokenizer
        self.task_prefix = task_prefix
        self.max_source_length = max_source_length
        self.max_target_length = max_target_length
        self.source_lang = source_lang
        self.target_lang = target_lang
        self.padding = padding
        self.num_workers = num_workers

    def setup(self, stage=None):
        self.train_dataset = CochraneDataset(
            list(self.dataset['train']),
            self.tokenizer,
            self.task_prefix,
            self.max_source_length,
            self.max_target_length,
            self.source_lang,
            self.target_lang,
            self.padding)

        self.val_dataset = CochraneDataset(
            list(self.dataset['validation']),
            self.tokenizer,
            self.task_prefix,
            self.max_source_length,
            self.max_target_length,
            self.source_lang,
            self.target_lang,
            self.padding)

        self.test_dataset = CochraneDataset(
            list(self.dataset['test']),
            self.tokenizer,
            self.task_prefix,
            self.max_source_length,
            self.max_target_length,
            self.source_lang,
            self.target_lang,
            self.padding)

    def train_dataloader(self):
        return DataLoader(
            self.train_dataset,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers
        )

    def test_dataloader(self):
        return DataLoader(
            self.test_dataset,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers
        )

    def val_dataloader(self):
        return DataLoader(
            self.test_dataset,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers
        )


class SASAutoencoderModel(pl.LightningModule):
    """
    An autoencoder built for scientific abstract simplification.
    """

    def __init__(self,
                 model_or_ckpt_name: str = MODEL_OR_CKPT_NAME,
                 # metrics: list = ["sacrebleu"],  # TODO: "bertscore", "meteor", "rouge"
                 tokenizer: str = MODEL_OR_CKPT_NAME,
                 ):
        super().__init__()
        self.model = T5ForConditionalGeneration.from_pretrained(model_or_ckpt_name)
        self.save_hyperparameters()
        self.metric_sacrebleu = datasets.load_metric('sacrebleu')
        self.metric_bertscore = datasets.load_metric('bertscore')
        self.metric_meteor = datasets.load_metric('meteor')
        self.tokenizer = T5TokenizerFast.from_pretrained(tokenizer)
        # self.p

    def forward(self, input_ids,
                attention_mask,
                # decoder_attention_mask,
                labels=None):
        output = self.model(
            input_ids=input_ids,
            attention_mask=attention_mask,
            labels=labels,
            # decoder_attention_mask=decoder_attention_mask
        )

        return output.loss, output.logits

    def training_step(self, batch, batch_size):
        input_ids = batch['input_ids']
        attention_mask = batch['attention_mask']
        labels = batch['labels']
        # labels_attention_mask = batch['labels_attention_mask']

        loss, outputs = self(
            input_ids=input_ids,
            attention_mask=attention_mask,
            # decoder_attention_mask=labels_attention_mask,
            labels=labels
        )
        self.log("train_loss", loss, batch_size=batch_size)
        wandb_logger.log_metrics({"train_loss": loss})
        return loss

    def validation_step(self, batch, batch_size):
        input_ids = batch['input_ids']
        attention_mask = batch['attention_mask']
        labels = batch['labels']
        # labels_attention_mask = batch['labels_attention_mask']

        loss, outputs = self(
            input_ids=input_ids,
            attention_mask=attention_mask,
            # decoder_attention_mask=labels_attention_mask,
            labels=labels
        )
        # the output is: odict_keys(['loss', 'logits', 'past_key_values', 'encoder_last_hidden_state'])

        # compute metrics
        abstracts = batch['source']
        references = [[r.strip()] for r in batch['target']]  # a list of lists
        output_sequences = self.model.generate(  # tensor of token ids
            input_ids=batch["input_ids"],
            attention_mask=batch["attention_mask"],
            max_length=512,
            do_sample=False,  # disable sampling to test if batching affects output
        )
        predictions = self.tokenizer.batch_decode(output_sequences, skip_special_tokens=True)
        predictions = [prediction.strip() for prediction in predictions]  # don't know if useful, will test [TODO]

        # log val_loss for checkpointing callbacks
        self.log("val_loss", loss, batch_size=batch_size)

        # TODO: preferably to get some global_step indicating where the samples are generated
        val_sacrebleu = self.metric_sacrebleu.compute(predictions=predictions, references=references)['score']
        wandb_logger.log_metrics({"val/sacrebleu": val_sacrebleu})
        # for checkpointing
        self.log("val_sacrebleu", val_sacrebleu, batch_size=batch_size)
        # wandb_logger.log_metrics({"val/bertscore_f1":
        #     np.mean(self.metric_bertscore.compute(predictions=predictions, references=references, lang='en')['f1'])})
        wandb_logger.log_metrics({"val/meteor":
                       self.metric_meteor.compute(predictions=predictions, references=references)['meteor']})
        wandb_logger.log_text(key=f"val_generated_texts/global_step_{self.global_step}",
                              columns=COLUMNS,
                              data=[*zip(predictions, abstracts, references)])
        wandb_logger.log_metrics({"val_loss": loss})

        return loss

    def test_step(self, batch, batch_size):
        input_ids = batch['text_input_ids']
        attention_mask = batch['text_attention_mask']
        labels = batch['labels']
        # labels_attention_mask = batch['labels_attention_mask']

        loss, outputs = self(
            input_ids=input_ids,
            attention_mask=attention_mask,
            # decoder_attention_mask=labels_attention_mask,
            labels=labels
        )

        # compute metrics
        abstracts = batch['source']
        references = [[r.strip()] for r in batch['target']]  # a list of lists
        output_sequences = self.model.generate(  # tensor of token ids
            input_ids=batch["input_ids"],
            attention_mask=batch["attention_mask"],
            do_sample=False,  # disable sampling to test if batching affects output
        )
        predictions = self.tokenizer.batch_decode(output_sequences, skip_special_tokens=True)
        predictions = [prediction.strip() for prediction in predictions]  # don't know if useful, will test [TODO]

        # log test_loss
        self.log("test_loss", loss, batch_size=batch_size)
        # TODO: preferably to get some global_step indicating where the samples are generated
        wandb_logger.log_metrics({"test/sacrebleu":
                       self.metric_sacrebleu.compute(predictions=predictions, references=references)['score']})
        # wandb_logger.log_metrics({"test/bertscore_f1",
        #            np.mean(self.metric_bertscore.compute(predictions=predictions, references=references, lang='en')['f1'])})
        wandb_logger.log_metrics({"test/meteor",
                   self.metric_meteor.compute(predictions=predictions, references=references)['meteor']})

        wandb_logger.log_text(key=f"test_generated_texts/global_step_{self.global_step}",
                              columns=COLUMNS,
                              data=[*zip(predictions, abstracts, references)])
        wandb_logger.log_metrics({"test_loss": loss})

        return loss

    def configure_optimizers(self):
        return torch.optim.AdamW(self.parameters(), lr=1e-4)


if __name__ == '__main__':

    # prepare the data
    dm = CochraneDataModule()

    # prepare the model
    model = SASAutoencoderModel(tokenizer=MODEL_OR_CKPT_NAME)

    # prepare the trainer
    trainer = pl.Trainer(
        # fast dev
        # fast_dev_run=300,
        # gpu
        gpus=[0,1,2,3],
        strategy='dp',
        # mixed precision training
        # precision=16, accelerator="gpu", devices=1,
        # step
        max_epochs=MAX_EPOCHS,
        log_every_n_steps=50,
        val_check_interval=0.5,
        callbacks=[ModelCheckpoint(
            dirpath=CKPTS_DIR,
            every_n_train_steps=1000,
            filename='{epoch:02d}-{global_step:02d}-{val_loss:.2f}-{val_sacrebleu:.2f}',
            save_top_k=5,
            verbose=True,
            monitor='val_loss',
            mode='min'),
            LearningRateMonitor("step"),
            EarlyStopping(monitor="val_loss", min_delta=1e-3, patience=PATIENCE, verbose=False,
                          mode="min"),
            DeviceStatsMonitor()],
        # misc
        deterministic=True,
        enable_progress_bar=True,
        progress_bar_refresh_rate=30,
        move_metrics_to_cpu=True,
        logger=wandb_logger,
        default_root_dir=CKPTS_DIR,
    )

    trainer.fit(model, dm)

