#!/usr/bin/env python
# coding=utf-8

__author__ = "hw56@indiana.edu"
# __version__ = "alpha"
__license__ = "ISC"

import os
import re
import gzip
import json
import string
import numpy as np
from tqdm import tqdm
from copy import deepcopy
from bs4 import BeautifulSoup
from multiprocessing import Pool
from sacremoses import MosesTokenizer, MosesDetokenizer


def extract_clean_subcorpus_from_en_gigaword3(agency_name='nyt', corpus_dir='../english-gigaword_3/'):
    """

    :param agency_name: str, one in ['afp', 'apw', 'cna', 'ltw', 'nyt', 'xin']
    :param corpus_dir: str, path to the LDC english-gigaword_3 corpus folder
    :return: list of str, clean paragraph extract from the `agency_name` corpus of gigword3. The data is naively
        deduplicated (with set()).
    """
    source = ['afp', 'apw', 'cna', 'ltw', 'nyt', 'xin']
    assert agency_name in source, print(f"Pass in one of {source}.")

    texts = []
    for blob in ['a', 'b']:
        for agency in [gz_file for gz_file in os.scandir(os.path.join(corpus_dir, 'gigaword_eng_3' + blob, 'data'))]:
            if agency.name == agency_name + "_eng":
                gz_files = [gz_file for gz_file in os.scandir(agency.path) if gz_file.name.endswith('gz')]
                for gz_file in gz_files:
                    with gzip.open(gz_file.path, "rb") as f:
                        _gz_file = f.read().decode("utf-8", errors='xmlcharrefreplace')
                        soup = BeautifulSoup(_gz_file, 'html.parser')
                        # rules:
                        # 1. \n → a whitespace
                        # 2. left double quote (``) → "
                        # 3. left single quote (`) → '
                        # 4. redundent whitespace at the beginning and the end
                        # 5. control code will be removed. Sting starts with "(STORY CAN END HERE" will be removed from here to the end.
                        # 6. string less than 10 characters (can remove control code like "(MORE)")
                        _texts = [re.sub('\n', ' ', elem.string).replace('``', '"').replace("`", "'").replace("''",
                                                                                                              '"').strip()
                                  for elem in soup.find_all('p')]
                        _texts = [text.strip() for text in _texts if "(story can end here" not in text.lower()]
                        _texts = [text for text in _texts if len(text) >= 10]
                        texts += _texts

    return list(set(texts))


def inject_noise_to_raw(text='',
                        clean_ratio=0.6,
                        char_deletion_ratio=0.1,
                        char_insertion_ratio=0.1,
                        char_substitution_ratio=0.1,
                        char_swap_ratio=0.1,
                        rng=np.random.default_rng(42)):
    """
    Injects characters to text. Note that, clean_ratio, char_deletion_ratio, char_insertion_ratio, and char_swap_ratio
        should add to 1. The function is a rough implementation of the synthetic noise described in Karpukhin et al.,
        2019 (https://aclanthology.org/D19-5506.pdf)

    :param text: str, a clean paragraph
    :param clean_ratio: float, % words won't pollute
    :param char_deletion_ratio: float, % words one of whose characters will be deleted
    :param char_insertion_ratio: float, % words who will be inserted a random character
    :param char_substitution_ratio: float, % words one of whose characters will be substituted
    :param char_swap_ratio: float, % words one of whose characters will be substituted; swap won't happen to the initial
        and last character of a word
    :param rng: a random number generator
    :return: dict, {'translation': {'en_clean': `text`, 'en_noisy': `text_new`}} by the convention found in Huggingface
        translation tasks
    """
    assert isinstance(text, str), print('Pass in raw text.')
    assert 1.0 - (
                clean_ratio + char_deletion_ratio + char_insertion_ratio + char_substitution_ratio + char_swap_ratio) <= 1e-9, print(
        "All kinds of ratios should add to 1.")
    moses_tokenizer, moses_detokenizer = MosesTokenizer(lang='en'), MosesDetokenizer(lang='en')

    word_tokens_new = []
    word_tokens = moses_tokenizer.tokenize(text, escape=False)
    for word_token in word_tokens:
        if len(word_token) == 1:
            word_token_ = word_token
        else:
            choose_noise_from_multinomial = list(zip(['do_nothing', 'delete', 'insert', 'substitute', 'swap'],
                                                     rng.multinomial(1, [clean_ratio,
                                                                         char_deletion_ratio,
                                                                         char_insertion_ratio,
                                                                         char_substitution_ratio,
                                                                         char_swap_ratio, ]).tolist()))
            noise_type = [operation for (operation, value) in choose_noise_from_multinomial if value == 1][0]
            if noise_type == 'do_nothing':
                word_token_ = word_token
            elif noise_type == 'delete':
                word_token_ = delete_char(word_token, rng)
            elif noise_type == 'insert':
                word_token_ = insert_char(word_token, rng)
            elif noise_type == 'substitute':
                word_token_ = substitute_char(word_token, rng)
            elif noise_type == 'swap':
                word_token_ = swap_char(word_token, rng)
            else:
                print(f'Unknown error for word token `{word_token}`.')   # this never happens
        word_tokens_new.append(word_token_)
    text_new = moses_detokenizer.detokenize(word_tokens_new)

    return {'translation': {'en_clean': text, 'en_noisy': text_new}}


def delete_char(word_token, rng):
    """
    Randomly delete a character from a string.
    """
    word_token_new = ''
    del_index = rng.choice([c for c in range(len(word_token))])
    for idx, ele in enumerate(word_token):
        if idx != del_index:
            word_token_new += ele
    return word_token_new


def get_char_noise(rng,
                   lowercase_ratio,
                   uppercase_ratio,
                   whitespace_ratio,
                   digit_ratio):
    choose_noise_from_multinomial = list(zip(['lowercase', 'uppercase', 'whitespace', 'digit'],
                  rng.multinomial(1, [lowercase_ratio,
                                      uppercase_ratio,
                                      whitespace_ratio,
                                      digit_ratio]).tolist()))
    noise_type = [operation for (operation, value) in choose_noise_from_multinomial if value==1][0]
    if noise_type == 'lowercase':
        noise = rng.choice(list(string.ascii_lowercase))
    elif noise_type == 'uppercase':
        noise = rng.choice(list(string.ascii_uppercase))
    elif noise_type == 'whitespace':
        noise = rng.choice(list(string.whitespace))
    elif noise_type == 'digit':
        noise = rng.choice(list(string.digits))
    else:
        print(f'Unknown error when searching {noise_type} noise.')

    return noise


def insert_char(word_token,
                rng,
                lowercase_ratio=0.7,
                uppercase_ratio=0.1,
                whitespace_ratio=0.1,
                digit_ratio=0.1):
    """
    Randomly insert a character to a string following the specified character type distributions.
    """
    word_token_char_list = list(word_token)
    idx_to_insert = rng.choice(len(word_token_char_list))

    noise = get_char_noise(rng,
                           lowercase_ratio=lowercase_ratio,
                           uppercase_ratio=uppercase_ratio,
                           whitespace_ratio=whitespace_ratio,
                           digit_ratio=digit_ratio)
    word_token_char_list.insert(idx_to_insert, noise)
    word_token_new = ''.join(word_token_char_list)
    return word_token_new


def substitute_char(word_token,
                    rng,
                    lowercase_ratio=0.7,
                    uppercase_ratio=0.1,
                    whitespace_ratio=0.1,
                    digit_ratio=0.1):
    """
    Randomly substitute a character in a string following the specified character type distributions.
    """
    word_token_char_list = list(word_token)
    idx_to_insert = rng.choice(len(word_token_char_list))
    idx_to_remove = idx_to_insert + 1
    char_to_be_substitute = word_token_char_list[idx_to_insert]
    noise = deepcopy(char_to_be_substitute)

    # prevent inject the same char in the same place
    while char_to_be_substitute == noise:
        noise = get_char_noise(rng=rng,
                               lowercase_ratio=lowercase_ratio,
                               uppercase_ratio=uppercase_ratio,
                               whitespace_ratio=whitespace_ratio,
                               digit_ratio=digit_ratio)

    word_token_char_list.insert(idx_to_insert, noise)
    word_token_char_list.pop(idx_to_remove)
    word_token_new = ''.join(word_token_char_list)

    return word_token_new


def swap_char(word_token,
              rng):
    """
    Randomly swap two characters in a string. May return the same word_token, e.g., `hello` → `hello` by swapping the `l`s.
    """

    def _swap_char(char_list_without_head_tail, idx_1, idx_2):
        swap_1 = char_list_without_head_tail.pop(idx_1)
        swap_2 = char_list_without_head_tail.pop(idx_2 - 1)
        char_list_without_head_tail.insert(idx_1, swap_2)
        char_list_without_head_tail.insert(idx_2, swap_1)

        return ''.join(char_list_without_head_tail)

    if len(word_token) <= 3:
        new_word_token = word_token
    else:
        # choose two indices other than the first and last to swap
        char_list_without_head_tail = list(word_token[1:-1])
        indices = rng.choice(range(len(char_list_without_head_tail)), size=2, replace=False, shuffle=False).tolist()
        indices.sort()
        idx_1, idx_2 = indices
        new_word_token = word_token[0] + _swap_char(char_list_without_head_tail, idx_1, idx_2) + word_token[-1]

    return new_word_token


def generate_clean_noise_pair(clean_texts):
    data_dict_list = []
    with Pool(processes=10) as pool:
        for clean_noisy_dict in tqdm(pool.imap_unordered(inject_noise_to_raw, clean_texts), total=len(clean_texts)):
            data_dict_list.append(clean_noisy_dict)
    return data_dict_list


def list2jsonl(data_dict_list, path):
    with open(path, 'w') as f:
        for line in data_dict_list:
            json.dump(line, f)
            f.write('\n')

