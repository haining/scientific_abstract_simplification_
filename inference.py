#!/usr/bin/env python
# coding=utf-8

__author__ = "hw56@indiana.edu"
# __version__ = "alpha"
__license__ = "ISC"

import os

import datasets
import numpy as np
from typing import Optional
import pytorch_lightning as pl
from torch.utils.data import Dataset, DataLoader
from pytorch_lightning.loggers import WandbLogger
from transformers import T5TokenizerFast, T5ForConditionalGeneration
from pytorch_lightning.callbacks import ModelCheckpoint, LearningRateMonitor, EarlyStopping, DeviceStatsMonitor
from train import CochraneDataset, CochraneDataModule, SASAutoencoderModel

CKPT_PATH = 'ckpts_efficient_t5/epoch=00-global_step=00-val_loss=0.53-val_sacrebleu=59.16.ckpt'

model = SASAutoencoderModel.load_from_checkpoint(CKPT_PATH)
model.freeze()

tokenizer = T5TokenizerFast.from_pretrained('google/t5-efficient-tiny-nl32')

# first 20 GIGA3-NYT testing samples
cochrane_dataset = datasets.load_dataset('GEM/cochrane-simplification')
cochrane_test_first5_source = cochrane_dataset['test'][:5]['source']
cochrane_test_first5_target = cochrane_dataset['test'][:5]['target']


def simply_text(noisy_texts: list,
                 references: Optional[list],
                 frozen_pl_model=model):
    """
    Simplifies a list of noisy English string. If a list of reference strings are given, outputs the sacrebleu score.
    """
    encoding = tokenizer(
        noisy_texts,
        max_length=512,
        padding='max_length',
        truncation=True,
        return_tensors='pt'
    )
    decoded_ids = frozen_pl_model.model.generate(
        input_ids=encoding['input_ids'],
        attention_mask=encoding['attention_mask'],
        max_length=512,
        # num_beams=2,
        # repetition_penalty=2.5,
        # length_penalty=1.0,
        # early_stopping=True
    )

    generated_text = [
        tokenizer.decode(decoded_id, skip_special_tokens=True, clean_up_tokenization_spaces=True)
        for decoded_id in decoded_ids
    ]
    if references:
        sacrebleu = datasets.load_metric('sacrebleu')
        sacrebleu_score = sacrebleu.compute(predictions=[noisy_texts], references=[references])['score']
        return {"generated_text": generated_text,
                "sacrebleu": sacrebleu_score}
    else:
        return {"generated_text": generated_text}

    # return "".join(generated_text)


simply_text(noisy_texts=cochrane_test_first5_source,
                 references=cochrane_test_first5_target,
                 frozen_pl_model=model)

